/*------------------------------
Variables globales
*/

//canvas layers
var canvas = document.getElementById('canvas')
var context = canvas.getContext('2d')

// Début et fin des cordes
const BEGIN_STRING = 20
const END_STRING = 600

const FIRST_STRING = 200
const LAST_STRING = 500

// Score (en px)
const PERFECT = 10
const GOOD = 25
const TOUCH = 50

const PERFECT_SCORE = 100
const GOOD_SCORE = 50
const TOUCH_SCORE = 20
const FAILED_SCORE = -20
const TIME_PRESSED_DISPLAY = 1000

const PRESSED_COLOR = '#8e3d1b'
const PERFECT_COLOR = '#009933'
const GOOD_COLOR = '#66cc00'
const TOUCH_COLOR = '#86b300'
const FAILED_COLOR = '#cc0000'
const NOTHING_COLOR = '#666699'

// Input utilisateur
const USER_INPUTS = {
    'string-0': 'a',
    'string-1': 'z',
    'string-2': 'e',
    'string-3': 'd',
}

const STRING_COLORS = [
    '#ff8080',
    '#009900',
    '#1a53ff',
    '#e6b800',
]

// Statistiques
const Stats = {
    MISSED: 0,
    NOTHING: 1,
    PERFECT: 2,
    GOOD: 3,
    TOUCH: 4,
    STREAK: 5
}

/*------------------------------
Classes
*/
class Animation {
    animation = null
    time = 0
    timer = 0

    constructor(time, animation) {
        this.time = time
        this.animation = animation
    }

    animate(t) {
        this.timer += t
        this.animation(this.timer)
    }

    isFinished() {
        return this.timer > this.time
    }
}

class Input {
    t

    constructor(t) {
        this.t = t
    }

    getTimeBeforeArrived() {
        return this.t - gameManager.timePassed
    }

    draw(x, y, color) {
        context.beginPath()
        context.arc(x, y, 10, 0, 2 * Math.PI)
        context.fillStyle = color
        context.fill()
        context.lineWidth = 3
        context.strokeStyle = 'black'
        context.stroke()
        context.closePath()
    }
}

class String {
    key = null
    x = 0
    color = 'black'
    inputs = []
    lengthTime
    loopTime
    maxTime
    animations = []
    pressTimer = 0

    constructor(key, x, color, lengthTime, loopTime, maxTime) {
        this.key = key
        this.x = x
        this.color = color
        this.lengthTime = lengthTime
        this.loopTime = loopTime
        this.maxTime = maxTime
    }

    setInputs(inputs) {
        this.inputs = inputs
    }

    removeInput(input) {
        this.inputs.splice(this.inputs.indexOf(input), 1)
        const next = input.t + this.loopTime
        if (next < this.maxTime) {
            this.inputs.push(new Input(next))
        }
    }

    animateText(text, color) {
        const x = (Math.floor(Math.random() * 10) - 5) * 10
        const y = (Math.floor(Math.random() * 10) - 5) * 10
        this.animations.push(new Animation(1000, t => {
            context.font = "20px Fantasy"
            context.fillStyle = color
            context.textAlign = "center"
            const r = t / 2000
            context.fillText(text, this.x + x * r, END_STRING + y * r)
        }))
    }

    removeAnimation(animation) {
        this.animations.splice(this.inputs.indexOf(animation), 1)
    }

    getPixelOfInput(input) {
        return END_STRING - (END_STRING - BEGIN_STRING) * (input.getTimeBeforeArrived() / this.lengthTime)
    }

    getMostCloseInput() {
        let min = Number.MAX_SAFE_INTEGER
        let inp = null
        for (const input of this.inputs) {
            const val = Math.abs(input.getTimeBeforeArrived())
            if (val < min) {
                min = val
                inp = input
            }
        }

        return inp
    }

    keydown(k) {
        if (k.key === this.key) {
            const input = this.getMostCloseInput()
            const val = input === null ? TOUCH + 1 : Math.abs(this.getPixelOfInput(input) - END_STRING)
            this.pressTimer = TIME_PRESSED_DISPLAY
            if (val < TOUCH) {
                this.removeInput(input)

                if (val < PERFECT) {
                    this.animateText('PARFAIT', PERFECT_COLOR)
                    gameManager.score += PERFECT_SCORE
                    gameManager.addStatistic(Stats.PERFECT)
                }
                else if (val < GOOD) {
                    this.animateText('BIEN', GOOD_COLOR)
                    gameManager.score += GOOD_SCORE
                    gameManager.addStatistic(Stats.GOOD)
                }
                else {
                    this.animateText('TOUCHÉ', TOUCH_COLOR)
                    gameManager.score += TOUCH_SCORE
                    gameManager.addStatistic(Stats.TOUCH)
                }
            }
            else {
                this.animateText('RIEN', NOTHING_COLOR)
                gameManager.addStatistic(Stats.NOTHING)
            }
        }
    }

    update(t) {
        let toRemove = []
        for (const input of this.inputs) {
            if (this.getPixelOfInput(input) > END_STRING + TOUCH) {
                this.animateText('RATÉ', FAILED_COLOR)
                gameManager.score += FAILED_SCORE
                gameManager.addStatistic(Stats.MISSED)
                toRemove.push(input)
            }
        }

        for (const input of toRemove) {
            this.removeInput(input)
        }

        this.pressTimer -= t
    }

    colorDiff() {
        const aRgbHex = this.color.substring(1, 6).match(/.{1,2}/g);
        const aRgb = [
            parseInt(aRgbHex[0], 16),
            parseInt(aRgbHex[1], 16),
            parseInt(aRgbHex[2], 16)
        ];

        const bRgbHex = PRESSED_COLOR.substring(1, 6).match(/.{1,2}/g);
        const bRgb = [
            parseInt(bRgbHex[0], 16),
            parseInt(bRgbHex[1], 16),
            parseInt(bRgbHex[2], 16)
        ];

        const percent = this.pressTimer / TIME_PRESSED_DISPLAY
        const r = Math.floor(aRgb[0] + percent * (bRgb[0] - aRgb[0]));
        const g = Math.floor(aRgb[1] + percent * (bRgb[1] - aRgb[1]));
        const b = Math.floor(aRgb[2] + percent * (bRgb[2] - aRgb[2]));

        return 'rgb(' + r + ', ' + g + ', ' + b + ')'
    }

    draw(t) {
        context.beginPath()
        context.moveTo(this.x, BEGIN_STRING)
        context.lineTo(this.x, END_STRING)
        context.lineWidth = 5
        context.fillStyle = this.pressTimer > 0 ? this.colorDiff() : this.color
        context.strokeStyle = "black"
        context.stroke()
        context.closePath()
        
        context.beginPath()
        context.arc(this.x, END_STRING, 20, 0, 2 * Math.PI)
        context.fill()
        context.lineWidth = 3
        context.strokeStyle = 'black'
        context.stroke()
        context.closePath()

        context.beginPath()
        context.font = "20px Fantasy"
        context.fillStyle = "black"
        context.textAlign = "center"
        context.fillText(this.key.toUpperCase(), this.x, END_STRING + 7)
        context.closePath()

        for (const input of this.inputs) {
            const pixel = this.getPixelOfInput(input)
            if (pixel > BEGIN_STRING) {
                input.draw(this.x, pixel, this.color)
            }
        }

        let toDelete = []
        for (const animation of this.animations) {
            animation.animate(t)
            if (animation.isFinished()) {
                toDelete.push(animation)
            }
        }

        for (const animation of toDelete) {
            this.removeAnimation(animation)
        }
    }
}

/*------------------------------
Handlers
*/

var gameManager = {
    strings: [],
    timePassed: 0,
    musicLength: 0,
    score: 0,
    started: false,
    audio: null,
    statistics: {},

    addStatistic(stat) {
        this.statistics.stats[stat] += 1
        if (stat === Stats.PERFECT) {
            this.statistics.currentStreak += 1
            if (this.statistics.stats[Stats.STREAK] < this.statistics.currentStreak) {
                this.statistics.stats[Stats.STREAK] = this.statistics.currentStreak
            }
        }
        else if (stat !== Stats.NOTHING) {
            this.statistics.currentStreak = 0
        }
    },
    keydown(key) {
        if (key.key === 'Enter') {
            if (this.started) {
                this.stop()
            }
            else {
                this.launch()
            }
        }
    },
    launch() {
        this.statistics = {
            stats: [0, 0, 0, 0, 0, 0],
            currentStreak: 0,
            score: 0,
            time: 0
        }
        this.launchMusic('sounds/song-1.mp3', song1)
    },
    stop() {
        this.statistics.score = this.score
        this.statistics.time = this.timePassed
        this.audio.pause()
        this.audio = null
        for (const s of this.strings) {
            game.removeGameObject(s)
        }
        this.strings = []
        this.score = 0
        this.started = false
    },
    launchMusic(music_name, song) {
        let inputs = []

        const gap = (LAST_STRING - FIRST_STRING) / (song.strings.length - 1)

        for (let i = 0; i < song.strings.length; ++i) {
            this.strings.push(new String(USER_INPUTS['string-' + i], FIRST_STRING + gap * i, STRING_COLORS[i], song.strings[i], song.loop, song.length))
            inputs.push([])
        }

        for (const input of song.inputs) {
            inputs[input.s].push(new Input(input.t))
        }

        for (let i = 0; i < song.strings.length; ++i) {
            game.addGameObject(this.strings[i])
            this.strings[i].setInputs(inputs[i])
        }

        this.audio = new Audio(music_name)
        this.musicLength = song.length
        this.timePassed = 0
        
        this.audio.addEventListener("canplaythrough", event => {
            this.audio.play()
            this.started = true
            this.timePassed = 0
        });
    },
    update(t) {
        if (this.started) {
            this.timePassed += t
            if (this.musicLength < this.timePassed) {
                this.stop()
            }
        }
    },
    draw() {
        if (this.started) {
            context.font = "25px Fantasy"
            context.fillStyle = "black"
            context.textAlign = "end"
            const min = Math.floor(this.timePassed / 60000)
            const sec = Math.floor((this.timePassed % 60000) / 1000)
            context.fillText(min + ":" + (sec < 10 ? '0' : '') + sec, canvas.width - 10, 30)

            context.font = "25px Fantasy"
            context.fillStyle = "black"
            context.textAlign = "start"
            context.fillText("Score = " + this.score + " pts", 10, canvas.height - 10)
            
            context.font = "15px Fantasy"
            context.textAlign = "end"
            context.fillText("Entrée pour quitter", canvas.width - 10, canvas.height - 10)
        }
        else if (this.statistics.time > 0) {
            context.font = "30px Fantasy"
            context.textAlign = "center"
            context.fillStyle = "black"
            context.fillText("SCORE : " + this.statistics.score, canvas.width / 2, canvas.height / 2 - 200)
            const min = Math.floor(this.statistics.time / 60000)
            const sec = Math.floor((this.statistics.time % 60000) / 1000)
            context.fillText("Temps passé : " + min + ":" + (sec < 10 ? '0' : '') + sec, canvas.width / 2, canvas.height / 2 - 170)

            context.font = "20px Fantasy"
            context.textAlign = "end"
            context.fillStyle = PERFECT_COLOR
            context.fillText("Parfaits :", canvas.width / 2, canvas.height / 2 - 100)
            context.fillStyle = GOOD_COLOR
            context.fillText("Bons :", canvas.width / 2, canvas.height / 2 - 70)
            context.fillStyle = TOUCH_COLOR
            context.fillText("Touchés :", canvas.width / 2, canvas.height / 2 - 40)
            context.fillStyle = FAILED_COLOR
            context.fillText("Ratés :", canvas.width / 2, canvas.height / 2 - 10)
            context.fillStyle = NOTHING_COLOR
            context.fillText("Inutiles :", canvas.width / 2, canvas.height / 2 + 20)
            context.fillStyle = "black"
            context.fillText("Meilleur enchainement parfait :", canvas.width / 2, canvas.height / 2 + 50)

            context.textAlign = "start"
            context.fillStyle = PERFECT_COLOR
            context.fillText(this.statistics.stats[Stats.PERFECT], canvas.width / 2 + 20, canvas.height / 2 - 100)
            context.fillStyle = GOOD_COLOR
            context.fillText(this.statistics.stats[Stats.GOOD], canvas.width / 2 + 20, canvas.height / 2 - 70)
            context.fillStyle = TOUCH_COLOR
            context.fillText(this.statistics.stats[Stats.TOUCH], canvas.width / 2 + 20, canvas.height / 2 - 40)
            context.fillStyle = FAILED_COLOR
            context.fillText(this.statistics.stats[Stats.MISSED], canvas.width / 2 + 20, canvas.height / 2 - 10)
            context.fillStyle = NOTHING_COLOR
            context.fillText(this.statistics.stats[Stats.NOTHING], canvas.width / 2 + 20, canvas.height / 2 + 20)
            context.fillStyle = "black"
            context.fillText(this.statistics.stats[Stats.STREAK], canvas.width / 2 + 20, canvas.height / 2 + 50)
            
            context.font = "30px Fantasy"
            context.fillStyle = "black"
            context.textAlign = "center"
            context.fillText("Appuyez sur Entrée pour rejouer !", canvas.width / 2, canvas.height / 2 + 150)
        }
        else {
            context.font = "30px Fantasy"
            context.fillStyle = "#ad2023"
            context.textAlign = "center"
            context.fillText("GUITAR VILAIN", canvas.width / 2, canvas.height / 2 - 30)

            context.font = "30px Fantasy"
            context.fillStyle = "black"
            context.textAlign = "center"
            context.fillText("Appuyez sur Entrée pour lancer le jeu !", canvas.width / 2, canvas.height / 2 + 10)
        }
    }
};

/*------------------------------
Animation and interactions
*/

var game = {
    timer: 0,
    launched: false,
    gameObjects: [],

    /**
     * Initialisation du jeu
     * Cette méthode est à appeler une seule fois (au chargement de la page par exemple)
     */
    initialize() {
        document.addEventListener('keydown', k => {
            this.tryFunction('keydown', k)
        });
        this.launch()
    },

    /**
     * Lance la boucle de jeu
     */
    launch() {
        this.launched = true
        this.tryFunction('start')
        window.requestAnimationFrame(this.loop.bind(this))
    },

    /**
     * Arrête la boucle de jeu
     */
    stop() {
        this.launched = true
    },

    /**
     * Boucle de jeu qui update tous les objets du jeu
     */
    loop(timestamp) {
        context.clearRect(0,0,canvas.width,canvas.height);
        this.tryFunction('update', timestamp - this.timer)
        this.tryFunction('draw', timestamp - this.timer)
        this.timer = timestamp
        if (this.launched !== false)
            window.requestAnimationFrame(this.loop.bind(this))
    },

    /**
     * Ajoute un objet au jeu
     */
    addGameObject(go) {
        this.tryFunctionOnObject(go, 'initialize')
        this.gameObjects.push(go)
    },

    /**
     * Supprime un objet du jeu
     */
    removeGameObject(go) {
        this.tryFunctionOnObject(go, 'removed')
        this.gameObjects.splice(this.gameObjects.indexOf(go), 1)
    },

    // "Private" method
    tryFunctionOnObject(go, f, ...args) {
        if (typeof go[f] === 'function')
            go[f](...args)
    },
    tryFunction(f, ...args) {
        for (const g of this.gameObjects)
            this.tryFunctionOnObject(g, f, ...args)
    }
}

/*------------------------------
Page Start
*/
game.addGameObject(gameManager);

window.onload = game.initialize.bind(game);